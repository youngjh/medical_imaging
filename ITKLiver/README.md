# ITKLiver

## Introduction

Using level set methods to segment the liver from MRI.

## Prerequisites

Insight Segmentation and Registration Toolkit (ITK), libconfig

## Usage

To compile, modify *CMakeLists.txt* as appropriate and then create a directory alongside *ITKLiver* named *ITKLiver_build*. Once inside *ITKLiver_build*,

    cmake -DITK_DIR=~/ITK/ITKbin ../ITKLiver
    make
    ./<executable> geodesicParams.txt
