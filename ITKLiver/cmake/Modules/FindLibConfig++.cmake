# Find libconfig++

find_path(LIBCONFIG++_INCLUDE_DIR libconfig.h++ /usr/local/Cellar/libconfig/1.5/include)
find_library(LIBCONFIG++_LIBRARY config++ /usr/local/Cellar/libconfig/1.5/lib)
if (LIBCONFIG++_INCLUDE_DIR AND LIBCONFIG++_LIBRARY)
   set(LIBCONFIG++_FOUND TRUE)
endif (LIBCONFIG++_INCLUDE_DIR AND LIBCONFIG++_LIBRARY)

